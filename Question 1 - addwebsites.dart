import 'dart:developer';
import 'dart:math';

import 'package:flutter/material.dart';

class AddWebsite extends StatefulWidget {
  const AddWebsite({Key? key}) : super(key: key);

  @override
  State<AddWebsite> createState() => _AddWebsiteState();
}

class _AddWebsiteState extends State<AddWebsite> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    _addwebsites() {
      final subject = subjectController.text;
      final location = locationController.text;

      final ref = FirebaseFirebaseStore.instance.collection("Websites").doc();
      return ref
      .set({"Subject Name": subject,"Location": location,});
      .then ((value) => log("Collection added"))
      .catchError((onError) => log(nError));
    }

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: subjectController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: (BorderRadius.circular((20)))),
                  hintText: "Enter Suject Name")),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
              controller: locationController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: (BorderRadius.circular((20)))),
                  hintText: "Enter location")),
        ),
        ElevatedButton(onPressed: () {}, child: Text("Create Website"))
      ],
    );
  }
}
